package de.weatherapp.data.interactor;

import de.weatherapp.core.RxSchedulerWrapper;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.Subscriptions;

/**
 * Created by emanuele on 24.06.16.
 */
public abstract class Interactor<T, E> {

    private Subscription mSubscription = Subscriptions.empty();
    private RxSchedulerWrapper mSchedulerWrapper;

    public Interactor(RxSchedulerWrapper schedulerWrapper) {
        mSchedulerWrapper = schedulerWrapper;
    }

    public void unsubscribe() {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }

    public void execute(Subscriber<T> subscriber) {
        unsubscribe();
        mSubscription = buildInteractorObservable()
                .subscribeOn(mSchedulerWrapper.getSubscribeOn())
                .observeOn(mSchedulerWrapper.getObserveOn())
                .subscribe(subscriber);
    }

    public abstract void setRequest(E request);

    protected abstract Observable<T> buildInteractorObservable();
}
