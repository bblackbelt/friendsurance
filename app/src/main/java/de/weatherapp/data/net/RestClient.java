package de.weatherapp.data.net;

import java.util.HashMap;
import java.util.Map;

import de.weatherapp.BuildConfig;
import de.weatherapp.data.entity.WeatherRequest;
import de.weatherapp.data.entity.Weather;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by emanuele on 22.06.16.
 */
public class RestClient {

    public interface WeatherApiService {
        @Headers("Accept: application/json")
        @GET("forecast")
        Observable<Weather> getWeather(@QueryMap final Map<String, String> query);
    }


    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";


    private final WeatherApiService mWeatherApiService;

    private static RestClient sRestClient;

    private RestClient() {

        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mWeatherApiService = restAdapter.create(WeatherApiService.class);
    }

    public static synchronized RestClient getInstance() {
        if (sRestClient == null) {
            sRestClient = new RestClient();
        }
        return sRestClient;
    }

    public Observable<Weather> getWeather(final WeatherRequest request) {
        Map<String, String> query = new HashMap<>();
        if (request.getCoords() != null) {
            query.put("lat", String.valueOf(request.getCoords().getLat()));
            query.put("lon", String.valueOf(request.getCoords().getLon()));
        } else if (request.getCity() != null) {
            query.put("q", request.getCity());
        } else {
            throw new IllegalArgumentException("Neither City nor coords provided");
        }
        query.put("APPID", BuildConfig.API_KEY);
        return mWeatherApiService.getWeather(query);
    }

}
