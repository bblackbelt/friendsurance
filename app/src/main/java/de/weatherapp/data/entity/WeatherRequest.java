package de.weatherapp.data.entity;

import de.weatherapp.data.net.RestClient;

/**
 * Created by emanuele on 03.07.16.
 */
public class WeatherRequest implements Request {
    private String city;
    private Coords coords;

    public Coords getCoords() {
        return coords;
    }

    public void setCoords(Coords coords) {
        this.coords = coords;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
