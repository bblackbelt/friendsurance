package de.weatherapp.data.entity;

/**
 * Created by emanuele on 03.07.16.
 */
public class Coords {
    private double lon;
    private double lat;


    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
