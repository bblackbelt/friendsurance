package de.weatherapp.data.entity;

import android.text.TextUtils;

import java.util.List;

import de.weatherapp.presentation.Utils;

/**
 * Created by emanuele on 03.07.16.
 */
public class WeatherForecast {

    private long dt;
    private MainWeatherInfo main;
    private List<WeatherInfo> weather;

    public List<WeatherInfo> getWeather() {
        return weather;
    }

    public void setWeather(List<WeatherInfo> weather) {
        this.weather = weather;
    }

    public MainWeatherInfo getMainWeatherInfo() {
        return main;
    }

    public void setMainWeatherInfo(MainWeatherInfo main) {
        this.main = main;
    }

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    @Override
    public String toString() {
        String tempText = " - ";
        if (main != null) {
            tempText = tempText.concat(main.toString());
        }
        tempText = tempText.concat(" - ").concat(getDescription());
        return Utils.getTime(dt * 1000).concat(tempText);
    }

    public String getDescription() {
        if (weather == null || weather.isEmpty()) {
            return null;
        }
        return weather.get(0).getDescription();
    }

    public String getIcon() {
        if (weather == null || weather.isEmpty()) {
            return null;
        }
        String icon = weather.get(0).getIcon();
        if (!TextUtils.isEmpty(icon)) {
            return icon.concat(".png");
        }
        return null;
    }
}
