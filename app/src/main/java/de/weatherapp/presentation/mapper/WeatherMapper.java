package de.weatherapp.presentation.mapper;

import de.weatherapp.data.entity.Weather;
import de.weatherapp.data.entity.WeatherForecast;
import de.weatherapp.presentation.Utils;
import de.weatherapp.presentation.model.LocationWeatherViewModel;

/**
 * Created by emanuele on 04.07.16.
 */
public class WeatherMapper extends BaseMapper<Weather, LocationWeatherViewModel> {
    @Override
    public LocationWeatherViewModel transform(Weather entity) {
        LocationWeatherViewModel weatherViewModel = new LocationWeatherViewModel();
        weatherViewModel.setCity(entity.getCity());
        for (WeatherForecast weatherForecast : entity.getList()) {
            String date = Utils.getFormattedDate(weatherForecast.getDt() * 1000);
            weatherViewModel.addWeatherInfo(date, weatherForecast);
        }
        return weatherViewModel;
    }
}
