package de.weatherapp.presentation.adapter;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import de.weatherapp.presentation.fragments.WeatherFragment;
import de.weatherapp.presentation.model.LocationWeatherViewModel;

/**
 * Created by emanuele on 07.07.16.
 */
public class LocationAdapter extends FragmentStatePagerAdapter {

    private List<LocationWeatherViewModel> mDataSet;

    public LocationAdapter(FragmentManager fm, List<LocationWeatherViewModel> dataSet) {
        super(fm);
        mDataSet = dataSet;
    }

    @Override
    public Fragment getItem(int position) {
        WeatherFragment fragment = new WeatherFragment();
        fragment.setWeatherData(mDataSet.get(position));
        return fragment;
    }

    @Override
    public int getCount() {
        return mDataSet == null ? 0 : mDataSet.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (mDataSet == null || mDataSet.isEmpty() || mDataSet.get(position).getCity() == null) {
            return null;
        }
        return mDataSet.get(position).getCity().getName();
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    public void addWeatherInfo(LocationWeatherViewModel data) {
        int index = mDataSet.indexOf(data);
        if (index == -1) {
            mDataSet.add(data);
        } else {
            mDataSet.set(index, data);
        }
        notifyDataSetChanged();
    }
}