package de.weatherapp.presentation.view;

/**
 * Created by emanuele on 03.07.16.
 */
public interface LoadDataView<T> {

    void setProgressIndicator(boolean active);


    void showError(String message);

}
