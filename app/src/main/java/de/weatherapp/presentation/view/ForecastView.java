package de.weatherapp.presentation.view;

import de.weatherapp.presentation.model.LocationWeatherViewModel;

/**
 * Created by emanuele on 03.07.16.
 */
public interface ForecastView extends LoadDataView {
    void renderWeather(LocationWeatherViewModel data);
}
