package de.weatherapp.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import java.util.ArrayList;
import java.util.List;

import de.weatherapp.R;
import de.weatherapp.core.RxSchedulerWrapperImpl;
import de.weatherapp.data.entity.City;
import de.weatherapp.data.entity.Coords;
import de.weatherapp.data.entity.WeatherRequest;
import de.weatherapp.data.interactor.WeatherInteractor;
import de.weatherapp.presentation.adapter.LocationAdapter;
import de.weatherapp.presentation.model.LocationWeatherViewModel;
import de.weatherapp.presentation.presenter.PresenterLoader;
import de.weatherapp.presentation.presenter.WeatherPresenter;
import de.weatherapp.presentation.presenter.WeatherPresenterFactory;
import de.weatherapp.presentation.view.ForecastView;

public class MainActivity extends AppCompatActivity implements ForecastView,
        LoaderManager.LoaderCallbacks<WeatherPresenter> {

    private static final int LOADER_ID = 101;

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private WeatherPresenter mPresenter;
    private LocationAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupViewPager(new ArrayList<LocationWeatherViewModel>());
        getSupportActionBar().setTitle(null);

        getSupportLoaderManager().initLoader(LOADER_ID, null, this);
    }

    private void setupViewPager(List<LocationWeatherViewModel> data) {

        ViewPager viewPager = (ViewPager) findViewById(R.id.locations);
        viewPager.setAdapter(mAdapter = new LocationAdapter(getSupportFragmentManager(), data));

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                WeatherRequest request = new WeatherRequest();
                City city = new City();
                request.setCity(place.getName().toString());
                Coords coords = new Coords();
                coords.setLat(place.getLatLng().latitude);
                coords.setLon(place.getLatLng().longitude);
                request.setCoords(coords);
                loadWeatherInfo(request);
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                showError(PlaceAutocomplete.getStatus(this, data).getStatusMessage());
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.setInteractor(new WeatherInteractor(new RxSchedulerWrapperImpl()));
        mPresenter.setView(this);
        mPresenter.onStart();
    }

    private void loadWeatherInfo(WeatherRequest request) {
        mPresenter.setView(this);
        mPresenter.loadWeather(request);
    }

    private void startPlacesActivity() {
        Intent intent = null;
        try {
            intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .build(this);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
            showError(e.getMessage());
            return;
        }
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    public void renderWeather(LocationWeatherViewModel data) {
        mAdapter.addWeatherInfo(data);
    }

    @Override
    public void setProgressIndicator(boolean active) {
        if (findViewById(R.id.loader_container) != null) {
            findViewById(R.id.loader_container).setVisibility(active ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void showError(String message) {
        Snackbar.make(findViewById(R.id.coordinator), message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                startPlacesActivity();
                break;
        }
        return true;
    }


    @Override
    public Loader<WeatherPresenter> onCreateLoader(int id, Bundle args) {
        return new PresenterLoader<>(this, new WeatherPresenterFactory());
    }

    @Override
    public void onLoadFinished(Loader<WeatherPresenter> loader, WeatherPresenter data) {
        mPresenter = data;
    }

    @Override
    public void onLoaderReset(Loader<WeatherPresenter> loader) {

    }
}
