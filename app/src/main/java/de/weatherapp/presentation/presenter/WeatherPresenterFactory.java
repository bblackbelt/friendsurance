package de.weatherapp.presentation.presenter;

/**
 * Created by emanuele on 13.07.16.
 */
public class WeatherPresenterFactory implements PresenterFactory<WeatherPresenter> {
    @Override
    public WeatherPresenter createPresenter() {
        return new WeatherPresenter();
    }
}
