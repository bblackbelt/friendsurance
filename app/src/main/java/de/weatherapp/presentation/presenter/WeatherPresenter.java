package de.weatherapp.presentation.presenter;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import de.weatherapp.data.entity.Weather;
import de.weatherapp.data.entity.WeatherRequest;
import de.weatherapp.data.interactor.DefaultSubscriber;
import de.weatherapp.data.interactor.Interactor;
import de.weatherapp.presentation.mapper.WeatherMapper;
import de.weatherapp.presentation.view.ForecastView;

/**
 * Created by emanuele on 03.07.16.
 */

public class WeatherPresenter implements Presenter {


    private final class WeatherSubscriber extends DefaultSubscriber<Weather> {

        @Override
        public void onCompleted() {
            hideLoadingView();
        }

        @Override
        public void onError(Throwable e) {
            hideLoadingView();
            showErrorMessage(e);
        }

        @Override
        public void onNext(Weather weather) {
            int index = mWeather.indexOf(weather);
            if (index == -1) {
                mWeather.add(weather);
            } else {
                mWeather.set(index, weather);
            }
            showWeather(weather);
        }
    }

    private List<Weather> mWeather = new ArrayList<>();
    private ForecastView mForecastView;
    private Interactor<Weather, WeatherRequest> mInteractor;
    private WeatherMapper mapper = new WeatherMapper();

    public WeatherPresenter() {
    }


    public void setInteractor(@NonNull Interactor<Weather, WeatherRequest> interactor) {
        mInteractor = interactor;
    }

    public void setView(@NonNull ForecastView view) {
        mForecastView = view;
    }

    public void loadWeather(WeatherRequest request) {
        showLoadingView();
        fetchData(request);
    }

    private void showLoadingView() {
        mForecastView.setProgressIndicator(true);
    }

    private void hideLoadingView() {
        mForecastView.setProgressIndicator(false);
    }

    private void showWeather(final Weather weather) {
        mForecastView.renderWeather(mapper.transform(weather));
    }


    private void showErrorMessage(Throwable e) {
        mForecastView.showError(TextUtils.isEmpty(e.getMessage()) ? "Error fetching the time table" : e.getMessage());
    }


    private void fetchData(WeatherRequest request) {
        mInteractor.setRequest(request);
        mInteractor.execute(new WeatherSubscriber());
    }

    @Override
    public void resume() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void unsubscribe() {
        if (mInteractor != null) {
            mInteractor.unsubscribe();
        }
    }

    @Override
    public void onStart() {
        for (Weather w : mWeather) {
            showWeather(w);
        }
    }

    @Override
    public void destroy() {
        unsubscribe();
    }
}