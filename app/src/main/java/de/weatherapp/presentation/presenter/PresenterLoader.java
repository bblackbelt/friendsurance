package de.weatherapp.presentation.presenter;

import android.content.Context;
import android.support.v4.content.Loader;

/**
 * Created by emanuele on 13.07.16.
 */
public class PresenterLoader<T extends Presenter> extends Loader<T> {

    private final PresenterFactory<T> factory;
    private T presenter;


    public PresenterLoader(Context context, PresenterFactory<T> factory) {
        super(context);
        this.factory = factory;
    }


    @Override
    protected void onStartLoading() {
        if (presenter != null) {
            deliverResult(presenter);
            return;
        }
        forceLoad();
    }

    @Override
    protected void onForceLoad() {
        presenter = factory.createPresenter();
        deliverResult(presenter);
    }

    @Override
    protected void onReset() {
        presenter.destroy();
        presenter = null;
    }
}
